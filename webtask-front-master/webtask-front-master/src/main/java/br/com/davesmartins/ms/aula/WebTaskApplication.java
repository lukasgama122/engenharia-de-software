package br.com.davesmartins.ms.aula;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@Slf4j
@SpringBootApplication
public class WebTaskApplication {

    public static void main(String[] args) {
        SpringApplication.run(WebTaskApplication.class, args);
    }

    @Bean
    public CommandLineRunner runner() {
        return (args) -> {


            log.info("####### Web Task Front - Started #######");
        };

    }

}
