/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.webtask.aula.domain.model;

import java.time.LocalDate;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author Lucas
 */
public class TaskTest {
    
    public TaskTest() {
    }

    @Test
    public void testSomeMethod() {
        int a = 3;
        int b = 8;
        
        int c = a + b;
        
        Assertions.assertEquals(11, c);
        
    }
    
    @Test
    public void vericarStatusAtrasado(){
        Task t = new Task(1l,"estudar", LocalDate.now().minusDays(2), null, null );
        
        EStatus status = t.getStatus();
        
                Assertions.assertEquals(EStatus.ATRASADO, status);

}
    @Test
    public void vericarStatusConcluido(){
        Task t = new Task(1l,"estudar", LocalDate.now().minusDays(2),LocalDate.now().minusDays(2), null );
        
        EStatus status = t.getStatus();
        
                Assertions.assertEquals(EStatus.CONCLUIDO_PRAZO, status);

}
    @Test
    public void vericarStatusConcluidoAtrasado(){
        Task t = new Task(1l,"estudar", LocalDate.now().minusDays(2),LocalDate.now().minusDays(1), null );
        
        EStatus status = t.getStatus();
        
                Assertions.assertEquals(EStatus.CONCLUIDO_ATRASADO, status);

}
    
}
