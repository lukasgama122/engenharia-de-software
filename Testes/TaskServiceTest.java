/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.webtask.aula.controller.service;

import br.com.webtask.aula.domain.model.Task;
import br.com.webtask.aula.domain.repo.TaskRepo;
import static java.lang.Character.UnicodeScript.values;
import java.time.LocalDate;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

/**
 *
 * @author Lucas
 */
@ExtendWith(SpringExtension.class)
@ActiveProfiles("test")
public class TaskServiceTest {
    @InjectMocks
    private TaskService tasks;
    @MockBean
    private TaskRepo tasksRepo;
    
    @BeforeEach
    public void setUp(){
    MockitoAnnotations.initMocks(this);
    }
    
    public TaskServiceTest() {
    }

    @Test
    public void testSomeMethod() throws Exception {
        
        
        Task t = new Task(1l,"abc", LocalDate.now().minusDays(2),null, null );
        Task t2 = new Task(1l,"abc", LocalDate.now().minusDays(2),LocalDate.now(), null );
        Mockito.when( tasksRepo.save(Mockito.any(Task.class))).thenReturn(t2);
        
        
        Task t1 = tasks.finalizar(t);
        
        Assertions.assertTrue(t1.isFinish());
    }
    
    @Test
    public void testSomeMethod1() {
        
        
        Task t = new Task(1l,"", LocalDate.now().minusDays(2),null, null );
        Task t2 = new Task(1l,"", LocalDate.now().minusDays(2),LocalDate.now(), null );
        Mockito.when( tasksRepo.save(Mockito.any(Task.class))).thenReturn(t2);
        
        
        Task t1;
        try {
            t1 = tasks.finalizar(t);
            Assertions.fail("deveria gerar erro");
        } catch (Exception ex) {
           Assertions.assertTrue(true);
            
        }
        
        
    }
    
}
