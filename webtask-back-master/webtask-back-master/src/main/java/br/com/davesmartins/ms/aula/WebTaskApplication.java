package br.com.davesmartins.ms.aula;

import br.com.davesmartins.ms.aula.model.Tarefa;
import br.com.davesmartins.ms.aula.repo.TarefaRepo;
import java.time.LocalDate;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@Slf4j
@SpringBootApplication
public class WebTaskApplication {

    public static void main(String[] args) {
        SpringApplication.run(WebTaskApplication.class, args);
    }

  @Autowired
    private TarefaRepo taskRepo;

    @Bean
    public CommandLineRunner runner() {
        return (args) -> {

            if (taskRepo.count() == 0) {
                Tarefa t1 = Tarefa.builder().dueDate(LocalDate.now().plusDays(5)).task("Estudar Arquitetura").build();
                Tarefa t2 = Tarefa.builder().dueDate(LocalDate.now().plusDays(1)).task("Estudar CI/CD").build();
                Tarefa t3 = Tarefa.builder().dueDate(LocalDate.now().plusDays(9)).task("Estudar Engenharia de Software").build();
                Tarefa t4 = Tarefa.builder().dueDate(LocalDate.now().plusDays(15)).task("Estudar Git").build();
                Tarefa t5 = Tarefa.builder().dueDate(LocalDate.now().plusDays(6)).task("Estudar Deploy").build();
                taskRepo.save(t1);
                taskRepo.save(t2);
                taskRepo.save(t3);
                taskRepo.save(t4);
                taskRepo.save(t5);
            }

            log.info("####### Web Task Backend - Started #######");
        };

    }

}
