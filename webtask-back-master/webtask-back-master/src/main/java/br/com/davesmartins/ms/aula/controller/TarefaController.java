package br.com.davesmartins.ms.aula.controller;

import br.com.davesmartins.ms.aula.exception.ValidationException;
import br.com.davesmartins.ms.aula.model.Tarefa;
import br.com.davesmartins.ms.aula.repo.TarefaRepo;
import br.com.davesmartins.ms.aula.utils.DateUtils;
import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping(value ="/todo")
public class TarefaController {

	@Autowired
	private TarefaRepo todoRepo;
	
	@GetMapping
	public List<Tarefa> findAll() {
		return todoRepo.findAll();
	}
	
	@PostMapping
	public ResponseEntity<Tarefa> save(@RequestBody Tarefa todo) throws ValidationException {
		if(todo.getTask() == null || todo.getTask() == "") {
			throw new ValidationException("Fill the task description");
		}
		if(todo.getDueDate() == null) {
			throw new ValidationException("Fill the due date");
		}
		if(!DateUtils.isEqualOrFutureDate(todo.getDueDate())) {
			throw new ValidationException("Due date must not be in past");
		}
		Tarefa saved = todoRepo.save(todo);
		return new ResponseEntity<Tarefa>(saved, HttpStatus.CREATED);
	}
	
	@DeleteMapping(value = "/{id}")
	@ResponseStatus(code=HttpStatus.NO_CONTENT)
	public void delete(@PathVariable Long id) {
		todoRepo.deleteById(id);
	}
	
}
