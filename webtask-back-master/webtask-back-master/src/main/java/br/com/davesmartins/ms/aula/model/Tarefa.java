package br.com.davesmartins.ms.aula.model;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Tarefa {

	@Id
	@GeneratedValue
	private Long id;
	
        @NotNull
	@Column(nullable = false)
	private String task;
	
	@Column(nullable = false)
	private LocalDate dueDate;
	
}
