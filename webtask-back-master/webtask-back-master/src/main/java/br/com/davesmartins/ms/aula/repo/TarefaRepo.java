package br.com.davesmartins.ms.aula.repo;

import br.com.davesmartins.ms.aula.model.Tarefa;
import org.springframework.data.jpa.repository.JpaRepository;


public interface TarefaRepo extends JpaRepository<Tarefa, Long>{

}
